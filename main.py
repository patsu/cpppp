# Example Application for Communication with C++ Socket Server
# Author: Patrick Suing
# Version: 1.0

# import modules
import socket
from ctypes import *


# class CStateData
class CStateData(Structure):
    _fields_ = [
        ('mTime', c_float),
        ('mTheta', c_float),
        ('mThetaInt', c_float),
        ('mTheta_d', c_float),
        ('mS', c_float),
        ('mS_d', c_float),
        ('mPsi3_d', c_float),
        ('mUT', c_float),
        ('mUF', c_float)
    ]


CMESSAGESIZE = 128


# class CMessage
class CMessage(Structure):
    _fields_ = [
        ('mEvent', c_uint32),
        ('mDataField', (c_ubyte * CMESSAGESIZE))
    ]

    def __init__(self):
        self.mEvent = 0
        self.mDataField = (c_ubyte * CMESSAGESIZE).from_buffer(bytearray([0] * CMESSAGESIZE))

    def getStateData(self):
        return CStateData.from_buffer(self.mDataField)


# class CSocket
class CSocket:
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.host = '127.0.0.1'
        self.port = 40000

    def connect(self):
        self.sock.connect((self.host, self.port))
        return True

    def receiveMessage(self):
        success = False
        bytes_recv = 0
        msgBuffer = bytearray(sizeof(CMessage))
        while bytes_recv < sizeof(CMessage):
            ret_val = self.sock.recv_into(msgBuffer, sizeof(CMessage))
            if ret_val < 0:
                print("[*] Python: receiveMessage(): Failed to read from socket")
                break
            elif ret_val == 0:
                success = False
                break
            else:
                bytes_recv += ret_val
        if bytes_recv == sizeof(CMessage):
            success = True
            msg = CMessage.from_buffer(msgBuffer)
        return msg

    def transmitMessage(self, msg):
        success = False
        bytes_written = 0
        msgBuffer = bytearray(msg)
        while bytes_written < sizeof(msg):
            ret_val = self.sock.send(msgBuffer)
            if ret_val < 0:
                print("[*] Python: transmitMessage(): Failed to send data from socket")
                break
            elif ret_val == 0:
                success = False
                break
            else:
                bytes_written += ret_val
        if bytes_written == sizeof(msg):
            success = True
        return success


def main():
    client = CSocket()
    if client.connect():
        while True:
            msg_recv = client.receiveMessage()
            stateVector = msg_recv.getStateData()
            tmp = (stateVector.mTime, stateVector.mTheta, stateVector.mThetaInt, stateVector.mTheta_d, stateVector.mS,
                   stateVector.mS_d, stateVector.mPsi3_d, stateVector.mUT, stateVector.mUF)
            print(tmp)


if __name__ == '__main__':
   main()
