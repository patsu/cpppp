/*
 * main.cpp
 *
 *  Created on: Dec 10, 2020
 *      Author: Patrick Suing
 */
#include <iostream>
#include <unistd.h> /* sleep */
#include "CServer.h"
#include "CMessage.h"
#include "CStateData.h"
using namespace std;

int main()
{
	CMessage msg{EEvent::ExperimentData};
	CStateData stateData;
	CServer mServer;
	mServer.init();
	cout << "[*] Server: Waiting for Client to connect to Server . . . " << endl;
	if(mServer.waitForClient() == true)
	{
		cout << "[*] Server: Client connected to Server . . . " << endl;
	}
	else
	{
		cout << "[*] Server: Failed to connect . . . " << endl;
	}

	while(true)
	{
		++stateData.mTime;
		++stateData.mTheta;
		++stateData.mThetaInt;
		++stateData.mTheta_d;
		++stateData.mS;
		++stateData.mS_d;
		++stateData.mPsi3_d;
		++stateData.mUT;
		++stateData.mUF;
		msg.setData(stateData);
		mServer.transmitMessage(msg);
		sleep(1);
	}

	return 0;
}
