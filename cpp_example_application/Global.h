/**
 * @author	Michael Meindl
 * @date	5.12.2016
 * @brief	Typedefs for POD
 */
#ifndef GLOBAL_H
#define GLOBAL_H
#include <cstdint>

using Int8   = int8_t;
using Int16  = int16_t;
using Int32  = int32_t;
using Int64  = int64_t;
using UInt8  = uint8_t;
using UInt16 = uint16_t;
using UInt32 = uint32_t;
using UInt64 = uint64_t;

#endif
