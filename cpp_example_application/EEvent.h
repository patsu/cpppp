// Alessandro Papa, 12.08.2020

#ifndef EEVENT_H
#define EEVENT_H
#include "Global.h"

enum class EEvent : UInt32
{
    DefaultIgnore = 0,
    Init,
    Exit,
    TimerTick,

    /* Communication Events */
    ClientConnected,
    ClientDisconnected,

    /* Data Events */
    ExperimentData,
	CalculatedData,

    /* Experiment Events */
    StopExperiment,
    StartMeasurement,
    StopMeasurement,
    RunE1Calibration,
	RunE2FeedbackControl,

	/* Control Events */
	EnableController,
	DisableController,
	InSpeedControlArea,
    ReloadConfiguration,
    EnableIntegralFeedback,
    DisableIntegralFeedback,

	/* ILC Events */
	SetupILC,
	CalculateILC,
	ILCExperimentFinished,
	ILCTrialFinished,
	RunILCTrial,
	RunILCDemoTrial,
	ILCExperimentFailed,
	SetNumberOfSamplesPerTrial,
	RunILCExperiment,
	SetNumberOfTrials,
	SetResetPositionAfterTrialFlag,

	/* QVI Events */
	SetupQVI,
	CalculateQVI,
	RunIteration,
	IterationFinished,
	SetNumberOfSamples,

	/* QPI Events */
	SetupQPI,
	CalculateQPI,

	/* Qt Events */
	SaveData
};

#endif
