/*
 * CStateData.cpp
 *
 *  Created on: 13.08.2020
 *      Author: Alessandro Papa
 */
#include "CStateData.h"

CStateData::CStateData() : mTime{0.0F},
                           mTheta{0.0F},
						   mThetaInt{0.0F},
						   mTheta_d{0.0F},
						   mS{0.0F},
						   mS_d{0.0F},
						   mPsi3_d{0.0F},
						   mUT{0.0F},
						   mUF{0.0F}
{

}




