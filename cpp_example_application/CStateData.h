/*
 * CStateData.h Chabo
 *
 *  Created on: 13.08.2020
 *      Author: Alessandro Papa
 */

#ifndef CONTROLLIBRARY_CSTATEDATA_H_
#define CONTROLLIBRARY_CSTATEDATA_H_
#include "Global.h"

class CStateData
{
public:
	float mTime;
	float mTheta;		/* pitch angle */
	float mThetaInt;	/* pitch angle integral */
	float mTheta_d;		/* pitch angle velocity */
	float mS;			/* robot distance driven */
	float mS_d;			/* robot speed */
	float mPsi3_d;		/* flywheel rotation speed */
	float mUT;			/* tires torque */
	float mUF;			/* flywheel torque */
public:
	CStateData();
	CStateData& operator=(const CStateData&) = default;
	CStateData(const CStateData&)            = default;
	~CStateData()                            = default;
};

#endif /* CONTROLLIBRARY_CSTATEDATA_H_ */
