# CPPPP (C++ plus Python)
## Starting point
An existing C++ application wants to communicate with a Python Client. Prerequisite was the use of a TCP/IP socket connection, since this was already implemented in C++. The existing C++ program is used to control mechatronic projects such as a two-wheeled inverted pendulum robots. Using a Python application, mathematical algorithms are to be implemented that enable machine learning on the robot.

Within the C++ application, a class named CMessage is used to exchange Data from one component to another. The class is composed of an event and a data field. The event is implemented as an enumeration and triggers corrosponding actions.  
Here is the definition of the class CMessage:
```c++
<CMessage.h>

#include "EEvent.h"
class CMessage
{
public:
	EEvent getEvent() const;
	template<class DataType>
	const DataType& getData() const;
	template<class DataType>
	void setData(const DataType&);
public:
	CMessage();
	CMessage(const EEvent&);
	CMessage& operator=(const CMessage&) = default;
	CMessage(const CMessage&)            = default;
	~CMessage()                          = default;
private:
	EEvent mEvent;
	UInt8  mDataField[cMessageSize];
};
template<class DataType>
const DataType& CMessage::getData() const
{
	static_assert(sizeof(DataType) < cMessageSize,
                  "CMessage: Size of data type exceeds message size.");
    return *reinterpret_cast<const DataType*>(mDataField);
}
template<class DataType>
void CMessage::setData(const DataType& data)
{
	static_assert(sizeof(DataType) < cMessageSize,
              	  "CMessage: Size of data type exceeds message size.");

	*reinterpret_cast<DataType*>(mDataField) = data;
}
```
In addition a part of the definition of EEvent.h:
```c++
<EEvent.h>

#include "Global.h" // redefiniton of POD types, because applications on different architectures / machines
enum class EEvent : UInt32
{
	DefaultIgnore = 0,
    Init,
    Exit,
    TimerTick,

    /* Communication Events */
    ClientConnected,
    ClientDisconnected,

    /* Data Events */
    ExperimentData,

	....

};
```
Using a TCP/IP socket, an object of type CMessage can be sent to another application. There is a class `CServer`, which has a method to send objects of type `CMessage`. This method takes a reference parameter of an object of type CMessage and sends this message using the POSIX function `send`. Because the TCP/IP socket sends data as bytes/bytestream, complex data structures (such as `CMessage`) have to be interpreted as bytes by the sender. Therefore the C++ conversion `reinterpret_cast<const UInt8*>` is used. In the header <Global.h> `UInt8` is defined as `using UInt8  = uint8_t`. The following is an excerpt from the CServer class showing the `transmitMessage` method.

```c++
<CServer.cpp>

#include "CServer.h"
#include "Global.h"
...

bool CServer::transmitMessage(const CMessage& msg)
{
	const UInt8* buffer = reinterpret_cast<const UInt8*>(&msg);

	...

	send(mConnectedSocketFD, (buffer+writtenByte), (sizeof(msg) - writtenByte), MSG_NOSIGNAL);

	...
}

...
```

## Implementation of Python client
![](cpp_python_cmessage.png)
### CMessage in Python
In the next step I had to implement a Python client that connects to TCP/IP socket server and then listens to incomming messages. My goal was to develop a receiving method that receives the bytestream and converts it into a data structure or class similar to `CMessage` on the C++ side. After some research I found the library `ctypes` for Python, which provides C compatible data types. With that library I was able to create a Python class which has the same attributes as its C++ counterpart.

```python
<main.py>

CMESSAGESIZE = 128
class CMessage(Structure):
    _fields_ = [
        ('mEvent', c_uint32),
        ('mDataField', (c_ubyte * CMESSAGESIZE))
    ]
```

The class `CMessage` derives from the base class `Structure` which is definded in the `ctypes` module. The attribute `mEvent` is defined as `c_uint32` which represents the C type `uint32_t` (cstdint). This corresponds to the definition of EEvent in C++ as `UInt32` - a `type alias` of `uint32_t`. This is defined in the header <Global.h> as `using UInt32 = uint32_t`. The attribute `mDataField` is defined as an array of 128 `c_ubyte`. This type represents the C type `usigned char` which again corresponds to the definition of `mDataField` on the C++ side in `CMessage` (`UInt8` -> `using UInt8  = uint8_t`).  

### Socket in Python
Afterwards the socket had to be implemented. There exists the Python module `socket`, which contains the basic functionality for socket-communication. I implemented a class named `CSocket` with basic functionaltiy to receive or transmit a message from type `cmessage`.

```python
# import own classes and files
from cmessage import *
# import modules
import socket

# class CSocket
class CSocket:
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.host = '127.0.0.1'
        self.port = 40000

    def connect(self):
        self.sock.connect((self.host, self.port))
        return True

    def receiveMessage(self):
        success = False
        bytes_recv = 0
        msgBuffer = bytearray(sizeof(CMessage))
        while bytes_recv < sizeof(CMessage):
            ret_val = self.sock.recv_into(msgBuffer, sizeof(CMessage))
            if ret_val < 0:
                print("[*] Python: receiveMessage(): Failed to read from socket")
                break
            elif ret_val == 0:
                success = False
                break
            else:
                bytes_recv += ret_val
        if bytes_recv == sizeof(CMessage):
            success = True
            msg = CMessage.from_buffer(msgBuffer)
        return msg

    def transmitMessage(self, msg):
        success = False
        bytes_written = 0
        msgBuffer = bytearray(msg)
        while bytes_written < sizeof(msg):
            ret_val = self.sock.send(msgBuffer)
            if ret_val < 0:
                print("[*] Python: transmitMessage(): Failed to send data from socket")
                break
            elif ret_val == 0:
                success = False
                break
            else:
                bytes_written += ret_val
        if bytes_written == sizeof(msg):
            success = True
        return success
```
There is an init method, which initializes the socket with the correct connection data from the socket server. In this case the server is located on the same machine and can therefore be reached via localhost. The port is 40000. Nn my use case the server (on C++ side) waits for incoming connections. As soon as the server is running, a connection can be established with the method `connect`.  
To receive a message from type `cmessage` a bytearray `msgBuffer` with the size of `cmessage` is initialized. This `msgBuffer` gets written until the number of received bytes matches the size of `cmessage`. If this case occurs (`bytes_recv == sizeof(CMessage)`) the method `CMessage.from_buffer` is called with the parameter `msgBuffer`. The return value is a ctypes instance of `cmessage`. This instance is then returned to the caller.  

### Reading data from received message (CMessage)
On the C++ side, any data types are written to the attribute `mDataField` using the template method `setData`. The user data is interpreted as the data type `uint_8` via `reinterpret_cast`. This makes it possible to store different data types in the attribute `mDataField`.  
The functionality of the data transmission of a message via the socket has already been described above. If a message has been received successfully by the Python client the attribut `mEvent` can be accessed directly through `msg.mEvent`. To interpret the attribut `mDataField` you need to know which data type has been sent. By evaluating the event, the corresponding data type can be assigned because Events and data types belong together in the later use case.  
Because `mDataField` is defined as an array of 128 `c_ubyte` you can use the method `from_buffer` to interpret this array as any data type.  
**For example:** This is the class `CStateData` in Python. This is just a structure of nine floats representing the state vector of a robot. The corrosponding C++ class is also given.

```python
<main.py>

class CStateData(Structure):
    _fields_ = [
        ('mTime', c_float),
        ('mTheta', c_float),
        ('mThetaInt', c_float),
        ('mTheta_d', c_float),
        ('mS', c_float),
        ('mS_d', c_float),
        ('mPsi3_d', c_float),
        ('mUT', c_float),
        ('mUF', c_float)
    ]
```
```c++
<CStateData.h>

class CStateData
{
public:
	float mTime;
	float mTheta;		/* pitch angle */
	float mThetaInt;	/* pitch angle integral */
	float mTheta_d;		/* pitch angle velocity */
	float mS;			/* robot distance driven */
	float mS_d;			/* robot speed */
	float mPsi3_d;		/* flywheel rotation speed */
	float mUT;			/* tires torque */
	float mUF;			/* flywheel torque */
public:
	CStateData();
	CStateData& operator=(const CStateData&) = default;
	CStateData(const CStateData&)            = default;
	~CStateData()                            = default;
};

```
An object of CStateData is send via socket from C++ to Python. We want to receive this message and do some calculation with it. With the methods given above it is possible to receive a bytestream and interpret this bytestream as `CMessage`. Now we want to "extract" the state vector from `mDataField`. To achieve this you can define a method `getStateData` in the Python class `CMessage`:

```python
<main.py>

CMESSAGESIZE = 128
class CMessage(Structure):
    _fields_ = [
        ('mEvent', c_uint32),
        ('mDataField', (c_ubyte * CMESSAGESIZE))
    ]

    def getStateData(self):
    return CStateData.from_buffer(self.mDataField)
```

This method returns an instance of `CStateData` interpreted from the array `mDataField`. The access to the attributes is then done via `stateVector.mTime`.  
## Test Applications
In this repository you can find test applications for Python an C++. First you have to compile the C++ sources and run the C++ binary. Afterwards the Python script can be started. The C++ Server just sends some dummy data to the Python client, which prints this data.
